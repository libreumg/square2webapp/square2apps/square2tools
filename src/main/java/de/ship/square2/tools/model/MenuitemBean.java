package de.ship.square2.tools.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author henkej
 *
 */
public class MenuitemBean implements Serializable {
	private static final long serialVersionUID = 5L;
	private String id;
	private String url;
	private String label;
	private String description;
	private final Set<String> roles;
	private Boolean isAlive;

	public MenuitemBean() {
		super();
		this.roles = new HashSet<>();
	}
	
	/**
	 * check if this role fits to the given ones
	 * 
	 * @param role the role to check
	 * @return true if the role is part of the internal ones; false otherwise
	 */
	public boolean hasRole(String role) {
		return this.roles.contains(role); // TODO: check for case sensitivity
	}

	/**
	 * check if this roles fits to any of the given ones
	 * 
	 * @param roles the roles to check
	 * @return true if at least one matches; false if none matches
	 */
	public boolean hasAnyRole(Set<String> roles) {
		for (String role : roles) {
			if (this.roles.contains(role)) { // TODO: check for case sensitivity
				return true; // speed up the seek; abort finding other matches
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder("Menuitem");
		buf.append("{id=").append(id);
		buf.append(", url=").append(url);
		buf.append(", label=").append(label);
		StringBuilder theRoles = new StringBuilder("{");
		for (String role : roles) {
			theRoles.append(role).append(",");
		}
		buf.append(", roles=").append(theRoles.append("}").toString());
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the roles
	 */
	public Set<String> getRoles() {
		return roles;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the isAlive
	 */
	public Boolean getIsAlive() {
		return isAlive;
	}

	/**
	 * @param isAlive the isAlive to set
	 */
	public void setIsAlive(Boolean isAlive) {
		this.isAlive = isAlive;
	}
}

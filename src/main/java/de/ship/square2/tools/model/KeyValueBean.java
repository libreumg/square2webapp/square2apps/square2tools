package de.ship.square2.tools.model;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class KeyValueBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer key;
	private String value;

	/**
	 * @return the key
	 */
	public Integer getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(Integer key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
